sap.ui.define([
    "sap/ui/core/mvc/Controller",
    'sap/ui/model/Filter',
    'sap/ui/model/FilterOperator'
],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
    function (Controller, Filter, FilterOperator) {
        "use strict";

        return Controller.extend("ns.HTML5Module.controller.View1", {
            onInit: function () {

            },
            onSearch: function (oEvent) {
                var oFilterBar = oEvent.getSource();
                var aFilterItems = oFilterBar.getAllFilterItems();
                var aFilters = [];

                var oControlNombre = oFilterBar.determineControlByName("nombre");
                if (oControlNombre.getValue() !== "") {
                    var oFilter = new Filter("asktx", FilterOperator.Contains, oControlNombre.getValue());
                    aFilters.push(oFilter);
                }

                var oControlCodigo = oFilterBar.determineControlByName("codigo");
                if (oControlCodigo.getValue() !== "") {
                    var oFilter = new Filter("asnum", FilterOperator.Contains, oControlCodigo.getValue());
                    aFilters.push(oFilter);
                }


                var oList = this.byId("idProductsTable");
                var oBinding = oList.getBinding("items");
                oBinding.filter(aFilters, "Application");


            }
        });
    });
